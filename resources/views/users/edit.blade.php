@extends('layouts.master')
@section('head.title', 'Edit User')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('users.edit', $user))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit User
        </div>
        {!! Form::model($user, ['route' => ['users.update', $user], 'method' => 'put']) !!}
            <div class="card-body">
                @include('users.partials.form', ['action' => 'update'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
