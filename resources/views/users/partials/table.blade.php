<table class="table table-bordered table-hover" id="users-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Created</th>
            <th>Last Updated</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td><input class="form-control form-control-sm"></td>
            <td><input class="form-control form-control-sm"></td>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td></td>
        </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#users-table', {
            ajax: "{{ route('users.dataTableData') }}",
            columns: [
                { data: 'name' },
                { data: 'email' },
                { data: 'created_at' },
                { data: 'updated_at' },
                { data: 'actions', searchable: false, sortable: false },
            ],
            order: [[2, 'desc']],
        });
    </script>
@endpush
