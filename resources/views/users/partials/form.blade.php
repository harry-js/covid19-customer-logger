{{ csrf_field() }}
<div class="form-group">
    {!! Form::label('name', __('Name')) !!}
    {!! Form::text('name', null, ['class' => 'form-control' . (($errors->has('name'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('name'))
        <span class="form-text invalid-feedback">{{ $errors->first('name') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('email', __('Email')) !!}
    {!! Form::email('email', null, ['class' => 'form-control' . (($errors->has('email'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('email'))
        <span class="form-text invalid-feedback">{{ $errors->first('email') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('password', __('Password')) !!}
    @php
    $passwordFieldOptions = ['class' => 'form-control' . (($errors->has('password'))? ' is-invalid' : '')];
    if ($action == 'store') {
        $passwordFieldOptions['required'] = true;
    }
    @endphp
    {!! Form::password('password', $passwordFieldOptions) !!}
    @if($errors->has('password'))
        <span class="form-text invalid-feedback">{{ $errors->first('password') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('password_confirmation', __('Password confirmation')) !!}
    @php
        $passwordConfirmationFieldOptions = ['class' => 'form-control' . (($errors->has('password_confirmation'))? ' is-invalid' : '')];
        if ($action == 'store') {
            $passwordConfirmationFieldOptions['required'] = true;
        }
    @endphp
    {!! Form::password('password_confirmation', $passwordConfirmationFieldOptions) !!}
    @if($errors->has('password_confirmation'))
        <span class="form-text invalid-feedback">{{ $errors->first('password_confirmation') }}</span>
    @endif
</div>