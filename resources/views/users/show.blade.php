@php
    /** @var \App\User $user */
@endphp
@extends('layouts.master')
@section('head.title', 'User Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('users.show', $user))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-user"></i> User Information
        </div>
        <div class="card-body">
            <dl>
                <dt>Name</dt>
                <dd>{{ $user->name }}</dd>
                <dt>Email</dt>
                <dd>{{ $user->email }}</dd>
                <dt>Created</dt>
                <dd>{{ $user->created_at->toDayDateTimeString() }}</dd>
                <dt>Last Updated</dt>
                <dd>{{ $user->updated_at->toDayDateTimeString() }}</dd>
            </dl>
        </div>
        @can('update', $user)
            <div class="card-footer">
                <a href="{{ route('users.edit', $user) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit User
                </a>
            </div>
        @endcan
    </div>
@endsection
