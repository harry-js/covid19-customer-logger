@extends('layouts.master')
@section('head.title', 'Users')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('users.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-users"></i> Users
        </div>
        <div class="card-body">
            @include('users.partials.table')
        </div>
        <div class="card-footer">
            <a href="{{ route('users.create') }}" class="btn btn-primary float-right">
                <i class="fas fa-plus"></i> Create User
            </a>
        </div>
    </div>
@endsection
