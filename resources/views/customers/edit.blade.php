@php
/**
 * @var \App\Customer $customer
 */
@endphp

@extends('layouts.master')
@section('head.title', 'Edit Customer')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('customers.edit', $customer))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit Customer
        </div>
        {!! Form::model($customer, ['route' => ['customers.update', $customer], 'method' => 'put']) !!}
            <div class="card-body">
                {{ csrf_field() }}
                <div class="form-group">
                    {!! Form::label('tested_positive_on', __('Tested positive on')) !!}
                    {!! Form::date('tested_positive_on', $customer->tested_positive_on ? $customer->tested_positive_on->format('Y-m-d') : null, ['class' => 'form-control' . (($errors->has('tested_positive_on'))? ' is-invalid' : '')]) !!}
                    <small class="form-text text-muted">If no date is selected, the customer will be considered as negative for covid-19. You can clear the date using the "clear date" button on the right side.</small>
                    @if($errors->has('tested_positive_on'))
                        <span class="form-text invalid-feedback">{{ $errors->first('tested_positive_on') }}</span>
                    @endif
                </div>
            </div>
            <div class="card-footer">
                <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
