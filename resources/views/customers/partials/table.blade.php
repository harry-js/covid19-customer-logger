<table class="table table-bordered table-hover" id="customers-table">
    <thead>
        <tr>
            <th>Name</th>
            <th>Visited</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    </tbody>
    <tfoot>
        <tr>
            <td><input class="form-control form-control-sm"></td>
            <td><input class="form-control form-control-sm" type="date"></td>
            <td></td>
        </tr>
    </tfoot>
</table>

@push('body.scripts')
    <script>
        app.fn.dataTable('table#customers-table', {
            ajax: "{{ route('customers.dataTableData') }}",
            columns: [
                { data: 'name' },
                { data: 'created_at' },
                { data: 'actions', searchable: false, sortable: false },
            ],
            order: [[1, 'desc']],
        });
    </script>
@endpush
