@php
/**
 * @var \App\Customer $customer
 */
@endphp

<div class="text-center">
    <div class="btn-group btn-group-sm">
        @can('view', $customer)
            <a href="{{ route('customers.show', $customer) }}" class="btn btn-light" title="View customer">
                <i class="fas fa-eye"></i>
            </a>
        @endcan
        @can('update', $customer)
            <a href="{{ route('customers.edit', $customer) }}" class="btn btn-primary" title="Edit customer">
                <i class="fas fa-edit"></i>
            </a>
        @endcan
    </div>
</div>
