{{ csrf_field() }}
<div class="form-group">
    {!! Form::label('name', __('Name *')) !!}
    {!! Form::text('name', null, ['class' => 'form-control' . (($errors->has('name'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('name'))
        <span class="form-text invalid-feedback">{{ $errors->first('name') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('email', __('Email *')) !!}
    {!! Form::email('email', null, ['class' => 'form-control' . (($errors->has('email'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('email'))
        <span class="form-text invalid-feedback">{{ $errors->first('email') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('phone', __('Phone')) !!}
    {!! Form::text('phone', null, ['class' => 'form-control' . (($errors->has('phone'))? ' is-invalid' : '')]) !!}
    @if($errors->has('phone'))
        <span class="form-text invalid-feedback">{{ $errors->first('phone') }}</span>
    @endif
</div>
