@extends('layouts.master')
@section('head.title', 'Log Customer')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('customers.create', $company))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-user"></i> Log Customer
        </div>
        {!! Form::model(new \App\Customer(), ['route' => ['customers.store', $company]]) !!}
            <div class="card-body">
                @include('customers.partials.form', ['action' => 'store'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
