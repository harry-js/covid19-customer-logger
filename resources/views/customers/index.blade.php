@extends('layouts.master')
@section('head.title', 'Customers')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('customers.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-customers"></i> Customers
        </div>
        <div class="card-body">
            @include('customers.partials.table')
        </div>
    </div>
@endsection
