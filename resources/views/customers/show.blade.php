@php
    /** @var \App\Customer $customer */
@endphp
@extends('layouts.master')
@section('head.title', 'Customer Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('customers.show', $customer))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-customer"></i> Customer Information
        </div>
        <div class="card-body">
            <dl>
                <dt>Name</dt>
                <dd>{{ $customer->name }}</dd>
                <dt>Email</dt>
                <dt><a href="mailto:{{ $customer->email }}">{{ $customer->email }}</a></dt>
                @if($customer->phone)
                    <dt>Phone</dt>
                    <dd>{{ $customer->phone }}</dd>
                @endif
                <dt>Tested Positive?</dt>
                <dd>{!! $customer->tested_positive_formatted !!}</dd>
                <dt>Visited</dt>
                <dd>{{ $customer->created_at->toDayDateTimeString() }}</dd>
            </dl>
        </div>
        @can('update', $customer)
            <div class="card-footer">
                <a href="{{ route('customers.edit', $customer) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit Customer
                </a>
            </div>
        @endcan
    </div>
@endsection
