@php
    /** @var \App\Company $company */
@endphp
@extends('layouts.master')
@section('head.title', 'Company Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('companies.show', $company))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-company"></i> Company Information
        </div>
        <div class="card-body">
            <dl>
                <dt>Name</dt>
                <dd>{{ $company->name }}</dd>
                @if($company->email)
                    <dt>Email</dt>
                    <dd>{{ $company->email }}</dd>
                @endif
                @if($company->phone)
                    <dt>Phone</dt>
                    <dd>{{ $company->phone }}</dd>
                @endif
                @if($company->address)
                    <dt>Address</dt>
                    <dd>{{ $company->address }}</dd>
                @endif
                <dt>Created</dt>
                <dd>{{ $company->created_at->toDayDateTimeString() }}</dd>
                <dt>Last Updated</dt>
                <dd>{{ $company->updated_at->toDayDateTimeString() }}</dd>
                <hr>
                <h2>Owner Information</h2>
                <dt>Name</dt>
                <dd>{{ $company->companyUser->user->name }}</dd>
                <dt>Email</dt>
                <dd>{{ $company->companyUser->user->email }}</dd>
                @if($company->companyUser->phone)
                    <dt>Phone</dt>
                    <dd>{{ $company->companyUser->phone }}</dd>
                @endif
            </dl>
        </div>
        @can('update', $company)
            <div class="card-footer">
                <a href="{{ route('companies.edit', $company) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit Company
                </a>
            </div>
        @endcan
    </div>
@endsection
