{{ csrf_field() }}
<div class="form-group">
    {!! Form::label('name', __('Name *')) !!}
    {!! Form::text('name', null, ['class' => 'form-control' . (($errors->has('name'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('name'))
        <span class="form-text invalid-feedback">{{ $errors->first('name') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('email', __('Email')) !!}
    {!! Form::email('email', null, ['class' => 'form-control' . (($errors->has('email'))? ' is-invalid' : '')]) !!}
    @if($errors->has('email'))
        <span class="form-text invalid-feedback">{{ $errors->first('email') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('phone', __('Phone')) !!}
    {!! Form::text('phone', null, ['class' => 'form-control' . (($errors->has('phone'))? ' is-invalid' : '')]) !!}
    @if($errors->has('phone'))
        <span class="form-text invalid-feedback">{{ $errors->first('phone') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('address', __('Address')) !!}
    {!! Form::text('address', null, ['class' => 'form-control' . (($errors->has('address'))? ' is-invalid' : '')]) !!}
    @if($errors->has('address'))
        <span class="form-text invalid-feedback">{{ $errors->first('address') }}</span>
    @endif
</div>
<h2>Owner Information</h2>
<div class="form-group">
    {!! Form::label('companyUser[user][name]', __('Name *')) !!}
    {!! Form::text('companyUser[user][name]', null, ['class' => 'form-control' . (($errors->has('companyUser.user.name'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('companyUser.user.name'))
        <span class="form-text invalid-feedback">{{ $errors->first('companyUser.user.name') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('companyUser[user][email]', __('Email *')) !!}
    {!! Form::email('companyUser[user][email]', null, ['class' => 'form-control' . (($errors->has('companyUser.user.email'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('companyUser.user.email'))
        <span class="form-text invalid-feedback">{{ $errors->first('companyUser.user.email') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('companyUser[phone]', __('Phone')) !!}
    {!! Form::text('companyUser[phone]', null, ['class' => 'form-control' . (($errors->has('companyUser.phone'))? ' is-invalid' : '')]) !!}
    @if($errors->has('companyUser.phone'))
        <span class="form-text invalid-feedback">{{ $errors->first('companyUser.phone') }}</span>
    @endif
</div>
