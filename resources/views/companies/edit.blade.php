@extends('layouts.master')
@section('head.title', 'Edit Company')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('companies.edit', $company))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-edit"></i> Edit Company
        </div>
        {!! Form::model($company, ['route' => ['companies.update', $company], 'method' => 'put']) !!}
            <div class="card-body">
                @include('companies.partials.form', ['action' => 'update'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
