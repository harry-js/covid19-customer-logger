@extends('layouts.master')
@section('head.title', 'Home')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('home'))
@section('body.content')
    <div class="jumbotron text-center">
        <h1 class="display-4">Covid19 Customer Logger</h1>
        <p class="lead">App to keep a log of your customers</p>
    </div>
@endsection
