@extends('layouts.master')
@section('head.title', 'Permissions')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('permissions.index'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-shield-alt"></i> Permissions
        </div>
        <div class="card-body">
            @include('permissions.partials.table')
        </div>
    </div>
@endsection
