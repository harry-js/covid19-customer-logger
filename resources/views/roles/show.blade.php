@php
    /** @var \Spatie\Permission\Models\Role $role */
@endphp
@extends('layouts.master')
@section('head.title', 'Role Information')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('roles.show', $role))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-gavel"></i> Role Information
        </div>
        <div class="card-body">
            <dl>
                <dt>Name</dt>
                <dd>{{ $role->name }}</dd>
                @if($permissions = $role->permissions->implode('name', ', '))
                    <dt>Permissions</dt>
                    <dd>{{ $permissions }}</dd>
                @endif
                <dt>Created</dt>
                <dd>{{ $role->created_at->toDayDateTimeString() }}</dd>
                <dt>Last Updated</dt>
                <dd>{{ $role->updated_at->toDayDateTimeString() }}</dd>
            </dl>
        </div>
        @can('update', $role)
            <div class="card-footer">
                <a href="{{ route('roles.edit', $role) }}" class="btn btn-primary float-right">
                    <i class="fas fa-edit"></i> Edit Role
                </a>
            </div>
        @endcan
    </div>
@endsection
