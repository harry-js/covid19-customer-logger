@extends('layouts.master')
@section('head.title', 'Create Role')
@section('body.breadcrumb', DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs::render('roles.create'))
@section('body.content')
    <div class="card">
        <div class="card-header">
            <i class="fas fa-gavel"></i> Create Role
        </div>
        {!! Form::model(new \Spatie\Permission\Models\Role(), ['route' => ['roles.store']]) !!}
            <div class="card-body">
                @include('roles.partials.form', ['action' => 'store'])
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        {!! Form::close() !!}
    </div>
@endsection
