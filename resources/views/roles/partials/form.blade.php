{{ csrf_field() }}
<div class="form-group">
    {!! Form::label('name', 'Name') !!}
    {!! Form::text('name', null, ['class' => 'form-control' . (($errors->has('name'))? ' is-invalid' : ''), 'required' => true]) !!}
    @if($errors->has('name'))
        <span class="form-text invalid-feedback">{{ $errors->first('name') }}</span>
    @endif
</div>
<div class="form-group">
    {!! Form::label('permissions', 'Permissions') !!}
    {!! Form::select('permissions[]', \Spatie\Permission\Models\Permission::query()->pluck('name', 'id'), null, ['class' => 'form-control' . (($errors->has('permissions'))? ' is-invalid' : ''), 'id' => 'permissions', 'multiple']) !!}
    @if($errors->has('permissions'))
        <span class="form-text invalid-feedback">{{ $errors->first('permissions') }}</span>
    @endif
</div>
@push('body.scripts')
    <script>
        $('#permissions').select2();
    </script>
@endpush
