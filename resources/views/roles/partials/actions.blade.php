<div class="text-center btn-group btn-group-sm">
    @can('view', $role)
        <a href="{{ route('roles.show', $role) }}" class="btn btn-light" title="View role">
            <i class="fas fa-eye"></i>
        </a>
    @endcan
    @can('update', $role)
        <a href="{{ route('roles.edit', $role) }}" class="btn btn-primary" title="Edit role">
            <i class="fas fa-edit"></i>
        </a>
    @endcan
    @can('delete', $role)
        <a href="javascript:void(0)" class="btn btn-danger btn-delete" title="Delete role" data-route="{{ route('roles.destroy', $role) }}" data-confirmation-text="Are you sure you want to delete the role?">
            <i class="fas fa-trash-alt"></i>
        </a>
    @endcan
</div>
