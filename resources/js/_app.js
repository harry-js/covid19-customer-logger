app.fn = {};

/**
 * @param tableSelector
 * @param options
 * @returns object|DataTables API object
 */
app.fn.dataTable = function (tableSelector, options = {}) {
    options = _.merge({
        processing: true,
        serverSide: true,
        responsive: true,
        initComplete: function () {
            let dataTable = this.api();

            dataTable.columns()
                .every(function() {
                    let that = this;

                    $('input', this.footer()).on('change', function() {
                        if (that.search() !== this.value) {
                            that.search(this.value).draw();
                        }
                    });
                });

            $('.btn-delete').on('click', function () {
                let $button = $(this);
                let confirmationText = $button.data('confirmation-text') ?
                    $button.data('confirmation-text') :
                    'Are you sure you want to perform the delete action?';

                if (confirm(confirmationText)) {
                    $.ajax({
                        type: $button.data('type') ?
                            $button.data('type') :
                            'DELETE',
                        url: $button.data('route'),
                        data: {
                            _token: $('meta[name="csrf-token"]').attr('content')
                        },
                        complete: function(){
                            dataTable.ajax
                                .reload();
                        }
                    });
                }
            });
        }
    }, options);

    return $(tableSelector).DataTable(options);
};

/**
 * @param $form
 */
app.fn.resetAjaxForm = function ($form) {
    $('input', $form).removeClass('is-invalid');
    $('.invalid-feedback', $form).remove();
};

/**
 * @param $form
 * @param errors
 */
app.fn.addValidationErrorMessagesToAjaxForm = function ($form, errors) {
    $.each(errors, function (field, errorMessages) {
        let $input = $('#' + field, $form);

        if ($input) { // input field found in form
            let invalidFeedback = '';

            errorMessages.forEach(function (errorMessage) {
                invalidFeedback += '<div class="invalid-feedback">' + errorMessage + '</div>';
            });

            $input.addClass('is-invalid');
            $input.after(invalidFeedback);
        }
    });
};

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('form.ajax-form').submit(function (e) {
    e.preventDefault();

    let $form = $(this);

    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: $form.serialize(),
        success: function (data, textStatus, jqXHR) {
            location.reload();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            app.fn.resetAjaxForm($form);
            app.fn.addValidationErrorMessagesToAjaxForm($form, jqXHR.responseJSON.errors);

        },
    });
});

$(function () {
    /**
     * Add clear data button to date input field. By default, date input field doesn't allows to clear its value.
     */

    // Append clear date button after the date input field.
    $('input[type=date]').after('<a type="button" class="clear-date-button float-right text-muted mt-1 small"><i class="fa fa-times"></i> Clear Date</a>');

    // Handle clear date button.
    $(document).on('click', 'a.clear-date-button', function (e) {
        e.preventDefault();

        let $dateInputField = $(this).prev();

        $dateInputField
            .val(null)
            .trigger('change');
    });
});
