<h2 align="center">Covid19 Customer Logger</h3>
<p align="center">
    <a href="https://gitlab.com/harry-js/covid19-customer-tracker/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/harry-js/covid19-customer-tracker/issues">Request Feature</a>
</p>

<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
  </ol>
</details>

## About The Project
Due to ongoing Covid-19 pandemic, many public authorities around the world have asked businesses to maintain record of 
customers visited their premises.

**Covid19 Customer Logger** is a SAAS solution for maintaining the log of customers visiting a business.


### Built With
* [Laravel](https://laravel.com)
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)

## Roadmap
See the [open issues](https://gitlab.com/harry-js/covid19-customer-tracker/issues) for a list of proposed features (and known issues).

## Contributing
Contributions are what make the open source community such an amazing place to learn, inspire and create. Any contributions you make are **greatly appreciated**.

## License
Distributed under the MIT License. See `LICENSE.txt` for more information.
