<?php

use App\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            $this->seedFakeCustomers();
        }
    }

    /**
     * @return void
     */
    private function seedFakeCustomers(): void
    {
        $maxCount = 20; // maximum number of customers per company
        $faker = Faker\Factory::create();

        Company::query()
            ->each(function (Company $company) use ($maxCount, $faker) {
                $count = $faker->numberBetween(0, $maxCount); // random number between 0 and "$maxCount"

                if ($count > 0) {
                    // Create customers for the company.

                    for ($i=0; $i<$count; $i++) {
                        $company
                            ->customers()
                            ->create([
                                'name' => $faker->name,
                                'email' => $faker->email,
                                'phone' => $faker->optional()->phoneNumber,
                                'tested_positive_on' => $faker->optional(0.05)->dateTimeBetween('-1 year'), // 5 % chance of testing positive
                            ]);
                    }
                }
            });
    }
}
