<?php

use App\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment('local')) {
            $this->seedFakeCompanies();
        }
    }

    /**
     * @return void
     */
    private function seedFakeCompanies(): void
    {
        $count = 20; // number of companies to seed
        $faker = Faker\Factory::create();

        for ($i=0; $i<$count; $i++) {
            // Create company.
            $company = new Company([
                'name' => $faker->unique()->company,
                'phone' => $faker->optional()->phoneNumber,
                'email' => $faker->optional()->companyEmail,
                'address' => $faker->optional()->address,
            ]);
            $company->save();

            // Create company user.
            $companyUser = $company
                ->companyUser()
                ->create([
                    'phone' => $faker->optional()->phoneNumber,
                ]);

            // Create user.
            $companyUser
                ->user()
                ->create([
                    'name' => $faker->name,
                    'email' => $faker->unique()->email,
                ]);
        }
    }
}
