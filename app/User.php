<?php

namespace App;

use App\Entities\Role as RoleEntity;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property CompanyUser|null $companyUser
 * @see User::companyUser()
 */
class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @var string
     */
    public const DEFAULT_PASSWORD = 'password';

    /**
     * @var string
     */
    public const SUPPORT_TEAM_EMAIL = 'support@covid19-customer-logger.test';

    /**
     * Set the user's password.
     *
     * @param  string  $value
     * @return void
     */
    public function setPasswordAttribute($value): void
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * Is the user super admin?
     *
     * @return bool
     */
    public function isSuperAdmin(): bool
    {
        return $this->hasRole(RoleEntity::SUPER_ADMIN);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isCompanyUser(): bool
    {
        return ! is_null($this->company_user_id);
    }

    /**
     * @return BelongsTo
     */
    public function companyUser()
    {
        return $this->belongsTo(CompanyUser::class);
    }

    /**
     * @return self|\Illuminate\Contracts\Auth\Authenticatable
     */
    public static function signedIn(): self
    {
        return Auth::user();
    }
}
