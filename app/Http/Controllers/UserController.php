<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Crud\UserCrudController;
use App\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class UserController extends UserCrudController
{
    /**
     * @return Factory|View
     */
    function index()
    {
        return view('users.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function dataTableData()
    {
        $query = User::query()
            ->select([
                'id',
                'name',
                'email',
                'created_at',
                'updated_at'
            ]);

        return Datatables::of($query)
            ->editColumn('created_at', function (User $user) {
                return $user->created_at->toDayDateTimeString();
            })
            ->editColumn('updated_at', function (User $user) {
                return $user->updated_at->toDayDateTimeString();
            })
            ->addColumn('actions', function (User $user) {
                return view('users.partials.actions', compact('user'));
            })
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @return Factory|View
     */
    function create()
    {
        return view('users.create');
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    /**
     * @param User $user
     * @return Factory|View
     */
    function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }
}
