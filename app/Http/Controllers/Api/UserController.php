<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Crud\UserCrudController;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserResourceCollection;
use App\User;
use Illuminate\Http\Request;

class UserController extends UserCrudController
{
    /**
     * @param Request $request
     * @return UserResourceCollection
     */
    public function index(Request $request)
    {
        $users = User::all();

        return new UserResourceCollection($users);
    }

    /**
     * @param User $user
     * @return UserResource
     */
    public function show(User $user)
    {
        return new UserResource($user);
    }
}
