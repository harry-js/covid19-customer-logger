<?php

namespace App\Http\Controllers;

use App\Company;
use App\Http\Controllers\Crud\CustomerCrudController;
use App\Customer;
use App\User;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class CustomerController extends CustomerCrudController
{
    /**
     * @return Factory|View
     */
    function index()
    {
        return view('customers.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function dataTableData()
    {
        $query = Customer::query()
            ->select([
                'id',
                'name',
                'company_id',
                'created_at',
                'tested_positive_on',
            ]);
        $signedInUser = User::signedIn();

        if ($signedInUser->isCompanyUser()) {
            $query->where('company_id', $signedInUser->companyUser->company_id);
        }

        return Datatables::of($query)
            ->editColumn('created_at', function (Customer $customer) {
                return $customer->created_at->toDayDateTimeString();
            })
            ->addColumn('actions', function (Customer $customer) {
                return view('customers.partials.actions', compact('customer'));
            })
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @param Company $company
     * @return Factory|View
     */
    function create(Company $company)
    {
        return view('customers.create', compact('company'));
    }

    /**
     * @param Customer $customer
     * @return Factory|View
     */
    function show(Customer $customer)
    {
        return view('customers.show', compact('customer'));
    }

    /**
     * @param Customer $customer
     * @return Factory|View
     */
    function edit(Customer $customer)
    {
        return view('customers.edit', compact('customer'));
    }
}
