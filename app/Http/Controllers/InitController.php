<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class InitController extends Controller
{
    /**
     * @return Factory|View
     */
    function home()
    {
        return view('home');
    }
}
