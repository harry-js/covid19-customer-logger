<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Crud\CompanyCrudController;
use App\Company;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;
use Yajra\DataTables\Facades\DataTables;

class CompanyController extends CompanyCrudController
{
    /**
     * @return Factory|View
     */
    function index()
    {
        return view('companies.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function dataTableData()
    {
        $query = Company::query()
            ->select([
                'id',
                'name',
                'created_at',
                'updated_at'
            ]);

        return Datatables::of($query)
            ->editColumn('created_at', function (Company $company) {
                return $company->created_at->toDayDateTimeString();
            })
            ->editColumn('updated_at', function (Company $company) {
                return $company->updated_at->toDayDateTimeString();
            })
            ->addColumn('actions', function (Company $company) {
                return view('companies.partials.actions', compact('company'));
            })
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @return Factory|View
     */
    function create()
    {
        return view('companies.create');
    }

    /**
     * @param Company $company
     * @return Factory|View
     */
    function show(Company $company)
    {
        return view('companies.show', compact('company'));
    }

    /**
     * @param Company $company
     * @return Factory|View
     */
    function edit(Company $company)
    {
        return view('companies.edit', compact('company'));
    }
}
