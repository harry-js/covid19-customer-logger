<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequestStore;
use App\Http\Requests\RoleRequestUpdate;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleController extends Controller
{
    /**
     * @return Factory|View
     */
    function index()
    {
        return view('roles.index');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    function dataTableData()
    {
        $query = Role::query()
            ->select([
                'id',
                'name',
                'created_at',
                'updated_at'
            ]);

        return Datatables::of($query)
            ->editColumn('created_at', function (Role $role) {
                return $role->created_at->toDayDateTimeString();
            })
            ->editColumn('updated_at', function (Role $role) {
                return $role->updated_at->toDayDateTimeString();
            })
            ->addColumn('actions', function (Role $role) {
                return view('roles.partials.actions', compact('role'));
            })
            ->rawColumns(['actions'])
            ->make();
    }

    /**
     * @return Factory|View
     */
    function create()
    {
        return view('roles.create');
    }

    /**
     * @param RoleRequestStore $request
     * @return RedirectResponse
     */
    function store(RoleRequestStore $request)
    {
        try {
            $role = new Role();
            $role->name = $request->input('name');
            $role->save();
            $role->permissions()->attach($request->input('permissions'));

            return redirect()->route('roles.show', $role);
        } catch (Exception $exception) {
            Log::error($exception->getMessage());

            return back();
        }
    }

    /**
     * @param Role $role
     * @return Factory|View
     */
    function show(Role $role)
    {
        return view('roles.show', compact('role'));
    }

    /**
     * @param Role $role
     * @return Factory|View
     */
    function edit(Role $role)
    {
        $role->permissions = $role->permissions->pluck('id'); // for select2 field

        return view('roles.edit', compact('role'));
    }

    /**
     * @param RoleRequestUpdate $request
     * @param Role $role
     * @return RedirectResponse
     */
    function update(RoleRequestUpdate $request, Role $role)
    {
        try {
            $role->name = $request->input('name');
            $role->save();
            $role->permissions()->sync($request->input('permissions', []));
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return redirect()->route('roles.show', $role);
    }

    /**
     * @param Role $role
     * @return JsonResponse
     */
    function destroy(Role $role)
    {
        try {
            $role->delete();
        } catch (Exception $exception) {
            Log::error($exception->getMessage());
        }

        return response()->json(['success' => true]);
    }
}
