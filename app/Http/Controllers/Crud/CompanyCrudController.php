<?php

namespace App\Http\Controllers\Crud;

use App\Actions\Crud\DeleteAction;
use App\Actions\Crud\StoreAction;
use App\Actions\Crud\UpdateAction;
use App\CompanyUser;
use App\Http\Requests\CompanyRequestStore;
use App\Http\Requests\CompanyRequestUpdate;
use App\Http\Resources\CompanyResource;
use App\Company;
use App\User;
use Illuminate\Http\Request;

abstract class CompanyCrudController extends CrudController
{
    /**
     * @param CompanyRequestStore $request
     * @return StoreAction
     */
    public function store(CompanyRequestStore $request)
    {
        $company = new Company();

        return (new StoreAction())
            ->setAction(function () use ($request, $company) {
                // Create company
                $company
                    ->fill($request->except('companyUser'))
                    ->save();

                // Create company user
                $companyUserInput = array_diff_key(
                    $request->input('companyUser'),
                    array_flip(['user'])
                ); // except user
                /** @var CompanyUser $companyUser */
                $companyUser = $company
                    ->companyUser()
                    ->create($companyUserInput);

                // Create user
                $companyUser
                    ->user()
                    ->create($request->input('companyUser.user'));

                return compact('company');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($company) {
                $this->logException($exception);

                return compact('company');
            })
            ->setResponse(function (Company $company) {
                return redirect()->route('companies.show', $company);
            })
            ->setExceptionResponse(function (Company $company) {
                return back();
            })
            ->setApiResponse(function (Company $company) {
                return new CompanyResource($company);
            });
    }

    /**
     * @param CompanyRequestUpdate $request
     * @param Company $company
     * @return UpdateAction
     */
    public function update(CompanyRequestUpdate $request, Company $company)
    {
        return (new UpdateAction())
            ->setAction(function () use ($request, $company) {
                // Update company
                $company->update($request->except('companyUser'));

                // Update company user
                $companyUserInput = array_diff_key(
                    $request->input('companyUser'),
                    array_flip(['user'])
                ); // except user
                /** @var CompanyUser $companyUser */
                $company
                    ->companyUser
                    ->update($companyUserInput);

                // Update user
                $company
                    ->companyUser
                    ->user
                    ->update($request->input('companyUser.user'));

                return compact('company');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($company) {
                $this->logException($exception);

                return compact('company');
            })
            ->setResponse(function (Company $company) {
                return redirect()->route('companies.show', $company);
            })
            ->setExceptionResponse(function (Company $company) {
                return back();
            })
            ->setApiResponse(function (Company $company) {
                return new CompanyResource($company);
            });
    }

    /**
     * @param Request $request
     * @param Company $company
     * @return DeleteAction
     */
    public function destroy(Request $request, Company $company)
    {
        return (new DeleteAction())
            ->setAction(function () use ($request, $company) {
                $company->delete();

                return compact('company');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($company) {
                $this->logException($exception);

                return compact('company');
            })
            ->setResponse(function (Company $company) {
                return $this->referredFromNamedRoute('companies.index') ?
                    response()->json(['success' => true]) :
                    redirect()->route('companies.index');
            })
            ->setExceptionResponse(function (Company $company) {
                return back();
            });
    }
}
