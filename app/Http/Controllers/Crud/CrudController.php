<?php

namespace App\Http\Controllers\Crud;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteCollection;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\URL;
use Throwable;

abstract class CrudController extends Controller
{
    /**
     * @param $name
     * @return bool
     */
    protected function referredFromNamedRoute($name): bool
    {
        /** @var RouteCollection $routeCollection */
        $routeCollection = Route::getRoutes();
        $route = $routeCollection->getByName($name);
        $referrer = URL::previous();
        $previousRequest = Request::create($referrer);

        return $route->matches($previousRequest);
    }

    /**
     * @param Throwable $exception
     * @return void
     */
    protected function logException(Throwable $exception): void
    {
        Log::error($exception->getMessage());
    }
}
