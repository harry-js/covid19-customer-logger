<?php

namespace App\Http\Controllers\Crud;

use App\Actions\Crud\DeleteAction;
use App\Actions\Crud\StoreAction;
use App\Actions\Crud\UpdateAction;
use App\Http\Requests\UserRequestStore;
use App\Http\Requests\UserRequestUpdate;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Http\Request;

abstract class UserCrudController extends CrudController
{
    /**
     * @param UserRequestStore $request
     * @return StoreAction
     */
    public function store(UserRequestStore $request)
    {
        $user = new User();

        return (new StoreAction())
            ->setAction(function () use ($request, $user) {
                $user->fill($this->getInput($request))
                    ->save();

                return compact('user');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($user) {
                $this->logException($exception);

                return compact('user');
            })
            ->setResponse(function (User $user) {
                return redirect()->route('users.show', $user);
            })
            ->setExceptionResponse(function (User $user) {
                return back();
            })
            ->setApiResponse(function (User $user) {
                return new UserResource($user);
            });
    }

    /**
     * @param UserRequestUpdate $request
     * @param User $user
     * @return UpdateAction
     */
    public function update(UserRequestUpdate $request, User $user)
    {
        return (new UpdateAction())
            ->setAction(function () use ($request, $user) {
                $user->update($this->getInput($request));

                return compact('user');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($user) {
                $this->logException($exception);

                return compact('user');
            })
            ->setResponse(function (User $user) {
                return redirect()->route('users.show', $user);
            })
            ->setExceptionResponse(function (User $user) {
                return back();
            })
            ->setApiResponse(function (User $user) {
                return new UserResource($user);
            });
    }

    /**
     * @param Request $request
     * @param User $user
     * @return DeleteAction
     */
    public function destroy(Request $request, User $user)
    {
        return (new DeleteAction())
            ->setAction(function () use ($request, $user) {
                $user->delete();

                return compact('user');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($user) {
                $this->logException($exception);

                return compact('user');
            })
            ->setResponse(function (User $user) {
                return $this->referredFromNamedRoute('users.index') ?
                    response()->json(['success' => true]) :
                    redirect()->route('users.index');
            })
            ->setExceptionResponse(function (User $user) {
                return back();
            });
    }

    /**
     * @param UserRequestStore $request
     * @return array
     */
    private function getInput(UserRequestStore $request)
    {
        $input = $request->all();

        if ($request->has('password')) {
            $input['password'] = bcrypt($request->input('password'));
        }

        return $input;
    }
}
