<?php

namespace App\Http\Controllers\Crud;

use App\Actions\Crud\StoreAction;
use App\Actions\Crud\UpdateAction;
use App\Company;
use App\Http\Requests\CustomerRequestStore;
use App\Http\Requests\CustomerRequestUpdate;
use App\Http\Resources\CustomerResource;
use App\Customer;

abstract class CustomerCrudController extends CrudController
{
    /**
     * @param CustomerRequestStore $request
     * @param Company $company
     * @return StoreAction
     */
    public function store(CustomerRequestStore $request, Company $company)
    {
        $customer = new Customer();

        return (new StoreAction())
            ->setAction(function () use ($request, $customer, $company) {
                $customer->fill($request->all());
                $customer->company()->associate($company);
                $customer->save();

                return compact('customer');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($customer) {
                $this->logException($exception);

                return compact('customer');
            })
            ->setResponse(function (Customer $customer) {
                return back();
            })
            ->setExceptionResponse(function (Customer $customer) {
                return back();
            })
            ->setApiResponse(function (Customer $customer) {
                return new CustomerResource($customer);
            });
    }

    /**
     * @param CustomerRequestUpdate $request
     * @param Customer $customer
     * @return UpdateAction
     */
    public function update(CustomerRequestUpdate $request, Customer $customer)
    {
        return (new UpdateAction())
            ->setAction(function () use ($request, $customer) {
                $customer->update($request->only('tested_positive_on'));

                return compact('customer');
            })
            ->setExceptionHandler(function (\Throwable $exception, $request) use ($customer) {
                $this->logException($exception);

                return compact('customer');
            })
            ->setResponse(function (Customer $customer) {
                return redirect()->route('customers.show', $customer);
            })
            ->setExceptionResponse(function (Customer $customer) {
                return back();
            })
            ->setApiResponse(function (Customer $customer) {
                return new CustomerResource($customer);
            });
    }
}
