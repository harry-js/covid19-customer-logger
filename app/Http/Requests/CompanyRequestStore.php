<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyRequestStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'unique:companies,name',
                'max:255',
            ],
            'email' => [
                'nullable',
                'email',
            ],
            'phone' => [
                'nullable',
                'max:255',
            ],
            'address' => [
                'nullable',
                'max:255',
            ],
            'companyUser.user.name' => [
                'required',
                'max:255',
            ],
            'companyUser.user.email' => [
                'required',
                'email',
                'unique:users,email',
            ],
            'companyUser.phone' => [
                'nullable',
                'max:255',
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function attributes()
    {
        return [
            'companyUser.user.name' => 'owner name',
            'companyUser.user.email' => 'owner email',
            'companyUser.phone' => 'owner phone',
        ];
    }
}
