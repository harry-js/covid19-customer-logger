<?php

namespace App\Http\Requests;

use App\Company;
use Illuminate\Validation\Rule;

class CompanyRequestUpdate extends CompanyRequestStore
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        /** @var Company $company */
        $company = $this->route('company');
        $rules = parent::rules();
        $rules['name'] = [
            'required',
            'max:255',
            Rule::unique('companies')->ignore($company),
        ];
        $rules['companyUser.user.email'] = [
            'required',
            'email',
            Rule::unique('users', 'email')->ignore($company->companyUser->user),
        ];

        return $rules;
    }
}
