<?php

namespace App\Http\Requests;

use App\User;
use Illuminate\Validation\Rule;

class UserRequestUpdate extends UserRequestStore
{
    /**
     * @inheritDoc
     */
    public function rules()
    {
        /** @var User $user */
        $user = $this->route('user');
        $rules = parent::rules();
        $rules['email'] = [
            'required',
            'email',
            Rule::unique('users')->ignore($user),
        ];
        $rules['password'] = [
            'nullable',
            'confirmed',
        ];

        return $rules;
    }
}
