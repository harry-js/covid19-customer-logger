<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

/**
 * @property Company $company
 * @see CompanyUser::company()
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $tested_positive_formatted
 * @see Customer::getTestedPositiveFormattedAttribute()
 * @property Carbon|null $tested_positive_on
 * @property string|null $tested_positive_on_formatted
 * @see Customer::getTestedPositiveOnFormattedAttribute()
 * @property ExposedCustomer|null $exposedCustomer
 * @see Customer::exposedCustomer()
 */
class Customer extends Model
{
    use SoftDeletes;
    use Notifiable;

    /**
     * @inheritdoc
     */
    protected $fillable = ['name', 'email', 'phone', 'tested_positive_on'];

    /**
     * @inheritdoc
     */
    protected $dates = ['tested_positive_on'];

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return bool
     */
    public function testedPositive(): bool
    {
        return ! is_null($this->tested_positive_on);
    }

    /**
     * @return string
     */
    public function getTestedPositiveFormattedAttribute()
    {
        return $this->testedPositive() ?
            "<span title='Tested positive on {$this->tested_positive_on_formatted}'>Yes</span>" :
            'No';
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getTestedPositiveOnFormattedAttribute()
    {
        return $this->testedPositive() ?
            $this->tested_positive_on->toFormattedDateString() :
            null;
    }

    /**
     * @return HasOne
     */
    public function exposedCustomer()
    {
        return $this->hasOne(ExposedCustomer::class);
    }

    /**
     * @return bool
     */
    public function isExposed(): bool
    {
        return is_a($this->exposedCustomer, ExposedCustomer::class);
    }
}
