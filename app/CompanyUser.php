<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property Company $company
 * @see CompanyUser::company()
 * @property User $user
 * @see CompanyUser::user()
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class CompanyUser extends Model
{
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $fillable = ['phone'];

    /**
     * @return BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * @return HasOne
     */
    public function user()
    {
        return $this->hasOne(User::class);
    }
}
