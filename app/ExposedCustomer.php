<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property Customer $customer
 * @see ExposedCustomer::customer()
 * @property Customer $exposedFromCustomer
 * @see ExposedCustomer::exposedFromCustomer()
 */
class ExposedCustomer extends Model
{
    /**
     * @inheritdoc
     */
    protected $fillable = ['exposed_on'];

    /**
     * @inheritdoc
     */
    protected $dates = ['exposed_on'];

    /**
     * @return BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * @return BelongsTo
     */
    public function exposedFromCustomer()
    {
        return $this->belongsTo(Customer::class);
    }
}
