<?php

namespace App\Observers;

use App\Customer;
use App\Events\CustomerTestedPositive;

class CustomerObserver
{
    /**
     * Handle the customer "updated" event.
     *
     * @param  \App\Customer  $customer
     * @return void
     */
    public function updated(Customer $customer)
    {
        if ($customer->wasChanged('tested_positive_on') && $customer->testedPositive()) {
            event(new CustomerTestedPositive($customer));
        }
    }
}
