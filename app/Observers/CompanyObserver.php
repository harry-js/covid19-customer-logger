<?php

namespace App\Observers;

use App\Company;
use Exception;

class CompanyObserver
{
    /**
     * Handle the company "deleted" event.
     *
     * @param Company $company
     * @return void
     * @throws Exception
     */
    public function deleted(Company $company)
    {
        // TODO check to make sure both are soft-deleted

        // Delete company user
        $company->companyUser()->delete();

        // Delete customers
        $company->customers()->delete();
    }
}
