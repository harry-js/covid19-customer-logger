<?php

namespace App\Listeners;

use App\Customer;
use App\Events\CustomerTestedPositive;
use App\ExposedCustomer;
use App\Notifications\ExposedCustomerNotification;
use Illuminate\Support\Collection;

class HandleExposedCustomers
{
    /**
     * Handle the event. Find out if there are any exposed customers. If there are, store the record for them and
     * send them email notification.
     *
     * @param  CustomerTestedPositive  $event
     * @return void
     */
    public function handle(CustomerTestedPositive $event)
    {
        $customerWhoTestedPositive = $event->customer;

        if ($customerWhoTestedPositive->tested_positive_on->isSameDay($customerWhoTestedPositive->created_at)) { // was the customer positive the day of his visit to the company?
            $exposedCustomers = $this->getExposedCustomers($customerWhoTestedPositive);

            $exposedCustomers
                ->take(2)
                ->each(function (Customer $customer) use ($customerWhoTestedPositive) {
                $this->addExposedCustomerRecord($customerWhoTestedPositive, $customer);

                $this->notifyExposedCustomer($customer);
            });
        }
    }

    /**
     * Get the collection of exposed customers. Customers who visited the company on the day when a customer tested
     * positive might have been exposed to covid-19.
     *
     * @param Customer $customerWhoTestedPositive
     * @return Collection
     */
    private function getExposedCustomers(Customer $customerWhoTestedPositive): Collection
    {
        return $customerWhoTestedPositive
            ->company
            ->customers()
            ->where('id' , '!=', $customerWhoTestedPositive->id) // exclude the customer who tested positive
            ->whereDate('created_at', '=', $customerWhoTestedPositive->tested_positive_on) // "created_at" is the date the customer visited the company
            ->get();
    }

    /**
     * Add record to track exposed customers.
     *
     * @param Customer $customerWhoTestedPositive
     * @param Customer $customer
     * @return void
     */
    private function addExposedCustomerRecord(Customer $customerWhoTestedPositive, Customer $customer): void
    {
        $exposedCustomer = new ExposedCustomer([
            'exposed_on' => $customerWhoTestedPositive->tested_positive_on->toDateString()
        ]);
        $exposedCustomer->customer()->associate($customer);
        $exposedCustomer->exposedFromCustomer()->associate($customerWhoTestedPositive);
        $exposedCustomer->save();
    }

    /**
     * Send email notification to exposed customer.
     *
     * @param Customer $customer
     * @return void
     */
    private function notifyExposedCustomer(Customer $customer): void
    {
        $customer->notify(new ExposedCustomerNotification());
    }
}
