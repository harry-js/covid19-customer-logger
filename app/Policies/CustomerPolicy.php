<?php

namespace App\Policies;

use App\Customer;
use App\Entities\Permission as PermissionEntity;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CustomerPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can(PermissionEntity::VIEW_CUSTOMERS) || $user->isCompanyUser();
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Customer $model
     * @return mixed
     */
    public function view(User $user, Customer $model)
    {
        return
            $user->can(PermissionEntity::VIEW_CUSTOMER) ||
            ($user->isCompanyUser() && ($model->company_id === $user->companyUser->company_id));
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Customer $model
     * @return mixed
     */
    public function update(User $user, Customer $model)
    {
        return $user->can(PermissionEntity::EDIT_CUSTOMER);
    }
}
