<?php

namespace App\Policies;

use App\Company;
use App\Entities\Permission as PermissionEntity;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param $ability
     * @return bool
     */
    public function before(User $user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->can(PermissionEntity::VIEW_COMPANIES);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->can(PermissionEntity::CREATE_COMPANY);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Company $model
     * @return mixed
     */
    public function view(User $user, Company $model)
    {
        return $user->can(PermissionEntity::VIEW_COMPANY);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Company $model
     * @return mixed
     */
    public function update(User $user, Company $model)
    {
        return $user->can(PermissionEntity::EDIT_COMPANY);
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Company $model
     * @return mixed
     */
    public function delete(User $user, Company $model)
    {
        return $user->can(PermissionEntity::DELETE_COMPANY);
    }
}
