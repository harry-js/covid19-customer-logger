<?php

namespace App\Actions;

use Closure;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

abstract class Action implements Responsable
{
    /**
     * @var bool
     */
    protected $wrapActionInTransaction = true;

    /**
     * @var Closure
     */
    protected $action;

    /**
     * @var mixed
     */
    protected $actionData;

    /**
     * @var bool
     */
    protected $error = false;

    /**
     * @var Closure
     */
    protected $handleException;

    /**
     * @var Closure
     */
    protected $exceptionResponse;

    /**
     * @var Closure
     */
    protected $response;

    /**
     * @var Closure
     */
    protected $apiResponse;

    /**
     * @var int
     */
    protected $status = Response::HTTP_OK;

    /**
     * @var int
     */
    protected $apiStatus = Response::HTTP_OK;

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @param int $apiStatus
     * @return $this
     */
    public function setApiStatus(int $apiStatus)
    {
        $this->apiStatus = $apiStatus;

        return $this;
    }

    /**
     * @return bool
     */
    protected function isApiRequest(): bool
    {
        return Auth::guard('api')->check();
    }

    /**
     * @return bool
     */
    public function hasError(): bool
    {
        return $this->error;
    }

    /**
     * @return bool
     */
    public function willWrapActionInTransaction(): bool
    {
        return $this->wrapActionInTransaction;
    }

    /**
     * @param bool $wrapActionInTransaction
     * @return $this
     */
    public function setWrapActionInTransaction(bool $wrapActionInTransaction)
    {
        $this->wrapActionInTransaction = $wrapActionInTransaction;

        return $this;
    }

    /**
     * @param Request $request
     * @return array
     * @throws Throwable
     */
    protected function runAction($request): array
    {
        if ($this->wrapActionInTransaction) {
            $return = DB::transaction(function () use ($request) {
                return ($this->action)($request);
            });
        } else {
            $return = ($this->action)($request);
        }

        return $return;
    }

    /**
     * @param Request $request
     * @return array
     * @throws Throwable
     */
    public function run($request): array
    {
        if ($this->isApiRequest()) {
            $return = $this->runAction($request);
        } else {
            try {
                $return = $this->runAction($request);
            } catch (Throwable $exception) {
                $this->error = true;
                $return = is_callable($this->handleException) ? ($this->handleException)($exception, $request) : [$exception, $request];
            }
        }

        return $return;
    }

    /**
     * @param Closure $action
     * @return $this
     */
    public function setAction(Closure $action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @param Closure $handleException
     * @return $this
     */
    public function setExceptionHandler(Closure $handleException)
    {
        $this->handleException = $handleException;

        return $this;
    }

    /**
     * @param Closure $response
     * @return $this
     */
    public function setResponse(Closure $response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * @param Closure $exceptionResponse
     * @return $this
     */
    public function setExceptionResponse(Closure $exceptionResponse)
    {
        $this->exceptionResponse = $exceptionResponse;

        return $this;
    }

    /**
     * @param Closure $apiResponse
     * @return $this
     */
    public function setApiResponse(Closure $apiResponse)
    {
        $this->apiResponse = $apiResponse;

        return $this;
    }

    /**
     * @return int
     */
    public function status()
    {
        return $this->isApiRequest() ? $this->apiStatus : $this->status;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws Throwable
     */
    public function toResponse($request)
    {
        $actionData = $this->run($request);

        if ($this->isApiRequest()) {
            if (is_callable($this->apiResponse)) {
                $response = call_user_func_array($this->apiResponse, $actionData);

                if (is_a($response, JsonResource::class)) {
                    $response = $response->toResponse($request);
                }
            } else {
                $response = response()->json();
            }
        } else {
            if ($this->hasError() && is_callable($this->exceptionResponse)) {
                $response = call_user_func_array($this->exceptionResponse, $actionData);
            } else {
                $response = call_user_func_array($this->response, $actionData);
            }
        }

        return $response;
    }
}
