<?php

namespace App\Actions;

class GenericAction extends Action
{
    /**
     * @inheritdoc
     */
    protected $wrapActionInTransaction = false;
}
