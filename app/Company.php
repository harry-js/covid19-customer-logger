<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property CompanyUser $companyUser
 * @see Company::companyUser()
 * @property Collection $customers
 * @see Company::customers()
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class Company extends Model
{
    use SoftDeletes;

    /**
     * @inheritdoc
     */
    protected $fillable = ['name', 'phone', 'email', 'address'];

    /**
     * @inheritdoc
     */
    protected $visible = ['name', 'phone', 'email', 'address', self::CREATED_AT, self::UPDATED_AT];

    /**
     * @return HasOne
     */
    public function companyUser()
    {
        return $this->hasOne(CompanyUser::class);
    }

    /**
     * @return HasMany
     */
    public function customers()
    {
        return $this->hasMany(Customer::class);
    }

    /**
     * @param false $withLink
     * @return string
     */
    public function getTitle($withLink = false): string
    {
        $title = $this->name;

        if ($withLink) {
            $title = link_to_route('companies.show', $title, [$this]);
        }

        return $title;
    }
}
