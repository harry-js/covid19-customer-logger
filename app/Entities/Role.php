<?php

namespace App\Entities;

class Role
{
    public const SUPER_ADMIN = 'Super Admin';

    /**
     * @var array
     */
    private const PERMISSIONS_MAP = [
        self::SUPER_ADMIN => [],
    ];

    /**
     * @return array
     */
    public static function getAll(): array
    {
        return [
            self::SUPER_ADMIN,
        ];
    }

    /**
     * Get permissions for a role.
     *
     * @param $role
     * @return array
     */
    public static function getPermissions($role): array
    {
        return self::PERMISSIONS_MAP[$role];
    }
}
