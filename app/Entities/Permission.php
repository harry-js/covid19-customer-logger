<?php

namespace App\Entities;

class Permission
{
    // User
    public const VIEW_USERS = 'User: Index';
    public const CREATE_USER = 'User: Create';
    public const VIEW_USER = 'User: Show';
    public const EDIT_USER = 'User: Edit';
    public const DELETE_USER = 'User: Delete';

    // Permission
    public const VIEW_PERMISSIONS = 'Permission: Index';
    public const VIEW_PERMISSION = 'Permission: Show';

    // Role
    public const VIEW_ROLES = 'Role: Index';
    public const CREATE_ROLE = 'Role: Create';
    public const VIEW_ROLE = 'Role: Show';
    public const EDIT_ROLE = 'Role: Edit';
    public const DELETE_ROLE = 'Role: Delete';

    // Company
    public const VIEW_COMPANIES = 'Company: Index';
    public const CREATE_COMPANY = 'Company: Create';
    public const VIEW_COMPANY = 'Company: Show';
    public const EDIT_COMPANY = 'Company: Edit';
    public const DELETE_COMPANY = 'Company: Delete';

    // Customer
    public const VIEW_CUSTOMERS = 'Customer: Index';
    public const VIEW_CUSTOMER = 'Customer: Show';
    public const EDIT_CUSTOMER = 'Customer: Edit';

    /**
     * @return array
     */
    public static function getAll(): array
    {
        return [
            // User
            self::VIEW_USERS,
            self::CREATE_USER,
            self::VIEW_USER,
            self::EDIT_USER,
            self::DELETE_USER,

            // Permission
            self::VIEW_PERMISSIONS,
            self::VIEW_PERMISSION,

            // Role
            self::VIEW_ROLES,
            self::CREATE_ROLE,
            self::VIEW_ROLE,
            self::EDIT_ROLE,
            self::DELETE_ROLE,

            // Role
            self::VIEW_COMPANIES,
            self::CREATE_COMPANY,
            self::VIEW_COMPANY,
            self::EDIT_COMPANY,
            self::DELETE_COMPANY,

            // Customer
            self::VIEW_CUSTOMERS,
            self::VIEW_CUSTOMER,
            self::EDIT_CUSTOMER,
        ];
    }
}
