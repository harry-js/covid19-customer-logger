<?php

namespace App\Providers;

use App\Company;
use App\Customer;
use App\Observers\CompanyObserver;
use App\Observers\CustomerObserver;
use App\Observers\UserObserver;
use App\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Company::observe(CompanyObserver::class);
        User::observe(UserObserver::class);
        Customer::observe(CustomerObserver::class);
    }
}
