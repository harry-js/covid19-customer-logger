<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Company;
use App\Customer;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

Auth::routes(['register' => false]);

// Init
Route::get('/', 'InitController@home')
    ->name('home');

// Customer
Route::get('companies/{company}/log-customer', 'CustomerController@create')
    ->name('customers.create');
Route::post('companies/{company}/customers', 'CustomerController@store')
    ->name('customers.store');

Route::middleware('auth')
    ->group(function () {
        Route::get('dashboard', 'DashboardController@index')
            ->name('dashboard');

        // User
        Route::get('users', 'UserController@index')
            ->name('users.index')
            ->middleware('can:index,' . User::class);
        Route::get('users/dataTableData', 'UserController@dataTableData')
            ->name('users.dataTableData')
            ->middleware('can:index,' . User::class);
        Route::get('users/create', 'UserController@create')
            ->name('users.create')
            ->middleware('can:create,' . User::class);
        Route::post('users', 'UserController@store')
            ->name('users.store')
            ->middleware('can:create,' . User::class);
        Route::get('users/{user}', 'UserController@show')
            ->name('users.show')
            ->middleware('can:view,user');
        Route::get('users/{user}/edit', 'UserController@edit')
            ->name('users.edit')
            ->middleware('can:update,user');
        Route::put('users/{user}', 'UserController@update')
            ->name('users.update')
            ->middleware('can:update,user');
        Route::delete('users/{user}', 'UserController@destroy')
            ->name('users.destroy')
            ->middleware('can:delete,user');

        // Permission
        Route::get('permissions', 'PermissionController@index')
            ->name('permissions.index')
            ->middleware('can:index,' . Permission::class);
        Route::get('permissions/dataTableData', 'PermissionController@dataTableData')
            ->name('permissions.dataTableData')
            ->middleware('can:index,' . Permission::class);
        Route::get('permissions/{permission}', 'PermissionController@show')
            ->name('permissions.show')
            ->middleware('can:view,permission');

        // Role
        Route::get('roles', 'RoleController@index')
            ->name('roles.index')
            ->middleware('can:index,' . Role::class);
        Route::get('roles/dataTableData', 'RoleController@dataTableData')
            ->name('roles.dataTableData')
            ->middleware('can:index,' . Role::class);
        Route::get('roles/create', 'RoleController@create')
            ->name('roles.create')
            ->middleware('can:create,' . Role::class);
        Route::post('roles', 'RoleController@store')
            ->name('roles.store')
            ->middleware('can:create,' . Role::class);
        Route::get('roles/{role}', 'RoleController@show')
            ->name('roles.show')
            ->middleware('can:view,role');
        Route::get('roles/{role}/edit', 'RoleController@edit')
            ->name('roles.edit')
            ->middleware('can:update,role');
        Route::put('roles/{role}', 'RoleController@update')
            ->name('roles.update')
            ->middleware('can:update,role');
        Route::delete('roles/{role}', 'RoleController@destroy')
            ->name('roles.destroy')
            ->middleware('can:delete,role');

        // Company
        Route::get('companies', 'CompanyController@index')
            ->name('companies.index')
            ->middleware('can:index,' . Company::class);
        Route::get('companies/dataTableData', 'CompanyController@dataTableData')
            ->name('companies.dataTableData')
            ->middleware('can:index,' . Company::class);
        Route::get('companies/create', 'CompanyController@create')
            ->name('companies.create')
            ->middleware('can:create,' . Company::class);
        Route::post('companies', 'CompanyController@store')
            ->name('companies.store')
            ->middleware('can:create,' . Company::class);
        Route::get('companies/{company}', 'CompanyController@show')
            ->name('companies.show')
            ->middleware('can:view,company');
        Route::get('companies/{company}/edit', 'CompanyController@edit')
            ->name('companies.edit')
            ->middleware('can:update,company');
        Route::put('companies/{company}', 'CompanyController@update')
            ->name('companies.update')
            ->middleware('can:update,company');
        Route::delete('companies/{company}', 'CompanyController@destroy')
            ->name('companies.destroy')
            ->middleware('can:delete,company');

        // Customer
        Route::get('customers', 'CustomerController@index')
            ->name('customers.index')
            ->middleware('can:index,' . Customer::class);
        Route::get('customers/dataTableData', 'CustomerController@dataTableData')
            ->name('customers.dataTableData')
            ->middleware('can:index,' . Customer::class);
        Route::get('customers/{customer}', 'CustomerController@show')
            ->name('customers.show')
            ->middleware('can:view,customer');
        Route::get('customers/{customer}/edit', 'CustomerController@edit')
            ->name('customers.edit')
            ->middleware('can:update,customer');
        Route::put('customers/{customer}', 'CustomerController@update')
            ->name('customers.update')
            ->middleware('can:update,customer');
    });
