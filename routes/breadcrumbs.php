<?php

use App\Company;
use App\Customer;
use App\User;
use DaveJamesMiller\Breadcrumbs\BreadcrumbsGenerator;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

// Init
Breadcrumbs::for('home', function (BreadcrumbsGenerator $trail) {
    $trail->push('Home', route('home'));
});

// Dashboard
Breadcrumbs::for('dashboard', function (BreadcrumbsGenerator $trail) {
    $trail->parent('home');
    $trail->push('Dashboard', route('dashboard'));
});

// User
Breadcrumbs::for('users.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Users', route('users.index'));
});
Breadcrumbs::for('users.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('users.index');
    $trail->push('Create User', route('users.create'));
});
Breadcrumbs::for('users.show', function (BreadcrumbsGenerator $trail, User $user) {
    $trail->parent('users.index');
    $trail->push($user->getTitle(), route('users.show', $user));
});
Breadcrumbs::for('users.edit', function (BreadcrumbsGenerator $trail, User $user) {
    $trail->parent('users.show', $user);
    $trail->push('Edit User', route('users.edit', $user));
});

// Permission
Breadcrumbs::for('permissions.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Permissions', route('permissions.index'));
});
Breadcrumbs::for('permissions.show', function (BreadcrumbsGenerator $trail, Permission $permission) {
    $trail->parent('permissions.index');
    $trail->push($permission->name, route('permissions.show', $permission));
});

// Role
Breadcrumbs::for('roles.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Roles', route('roles.index'));
});
Breadcrumbs::for('roles.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('roles.index');
    $trail->push('Create Role', route('roles.create'));
});
Breadcrumbs::for('roles.show', function (BreadcrumbsGenerator $trail, Role $role) {
    $trail->parent('roles.index');
    $trail->push($role->name, route('roles.show', $role));
});
Breadcrumbs::for('roles.edit', function (BreadcrumbsGenerator $trail, Role $role) {
    $trail->parent('roles.show', $role);
    $trail->push('Edit Role', route('roles.edit', $role));
});

// Company
Breadcrumbs::for('companies.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Companies', route('companies.index'));
});
Breadcrumbs::for('companies.create', function (BreadcrumbsGenerator $trail) {
    $trail->parent('companies.index');
    $trail->push('Create Company', route('companies.create'));
});
Breadcrumbs::for('companies.show', function (BreadcrumbsGenerator $trail, Company $company) {
    $trail->parent('companies.index');
    $trail->push($company->name, route('companies.show', $company));
});
Breadcrumbs::for('companies.edit', function (BreadcrumbsGenerator $trail, Company $company) {
    $trail->parent('companies.show', $company);
    $trail->push('Edit Company', route('companies.edit', $company));
});

// Customer
Breadcrumbs::for('customers.index', function (BreadcrumbsGenerator $trail) {
    $trail->parent('dashboard');
    $trail->push('Customers', route('customers.index'));
});
Breadcrumbs::for('customers.create', function (BreadcrumbsGenerator $trail, Company $company) {
    $trail->parent('home');
    $trail->push($company->name);
    $trail->push('Log Customer', route('customers.create', $company));
});
Breadcrumbs::for('customers.show', function (BreadcrumbsGenerator $trail, Customer $customer) {
    if (User::signedIn()->isCompanyUser()) {
        $trail->parent('dashboard');
    } else {
        $trail->parent('companies.show', $customer->company);
    }

    $trail->push('Customers');
    $trail->push($customer->name, route('customers.show', $customer));
});
Breadcrumbs::for('customers.edit', function (BreadcrumbsGenerator $trail, Customer $customer) {
    $trail->parent('customers.show', $customer);
    $trail->push('Edit Customer', route('customers.edit', $customer));
});
