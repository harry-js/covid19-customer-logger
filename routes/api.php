<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')
    ->group(function () {
        // User
        Route::get('users', 'UserController@index')
            ->name('users.index');
        Route::post('users', 'UserController@store')
            ->name('users.store');
        Route::get('users/{user}', 'UserController@show')
            ->name('users.show');
        Route::put('users/{user}', 'UserController@update')
            ->name('users.update');
        Route::delete('users/{user}', 'UserController@destroy')
            ->name('users.destroy');
    });
